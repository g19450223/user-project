import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useloginStore = defineStore('login', () => {
  const loginname = ref("");
const islogin = computed(() => {
  return loginname.value !== '';
});
const login = (username: string):void => {
  loginname.value = username;
  localStorage.setItem("loginname",username);
}
const logout = () => {
  loginname.value = "";
  localStorage.removeItem("loginname");
};
const loadData = () => {
  loginname.value = localStorage.getItem("loginname") || "";
}

  return { loginname ,islogin ,login ,logout ,loadData }
})
